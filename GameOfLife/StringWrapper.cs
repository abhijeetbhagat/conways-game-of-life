﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;

namespace GameOfLife
{
    public class StringWrapper : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<string> _values = new ObservableCollection<string>();
        public List<bool> Flags { get; set; }

        public string this[int i]
        {
            get
            {
              return _values[i];
            }

            set
            {
                if (_values.Count <  i + 1)
                {
                    _values.Add(value);
                }
                else
                {
                    _values[i] = value;
                }
                
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(Binding.IndexerName));
                }
            }
        }


    }
}
