﻿/*
    Copyright 2013 abhijeet bhagat
 
    Conway's Game Of Life is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Conway's Game Of Life is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Conway's Game Of Life.  If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Threading;

namespace GameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string DEAD = "";
        const string ACTIVE = "a";
        const int R = 25;
        const int C = 25;

        List<Tuple<int, int>> store = new List<Tuple<int, int>>();

        public MainWindow()
        {
            InitializeComponent();

            for (int i = 0; i < R; i++)
            {
                var sw = new StringWrapper();
                sw.Flags = new List<bool>();
                for (int j = 0; j < C; j++)
                {
                    sw[j] = DEAD;
                    sw.Flags.Add(false);

                }

                var column = new DataGridTextColumn();
                Style style = new System.Windows.Style(typeof(DataGridCell));
                Binding binding = new Binding("[" + i + "]");
                binding.Converter = new ValueConverter();
                binding.Mode = BindingMode.TwoWay;
                style.Setters.Add(new Setter(BackgroundProperty, binding));
                style.Setters.Add(new EventSetter(PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(Cell_Click)));

                column.CellStyle = style;
                column.Width = 10;

                worldGrid.Columns.Add(column);
                Rows.Add(sw);
            }

            //SetGliderPattern();
            //SetSmallExploderPattern();
            SetFlippingBar();

            worldGrid.ItemsSource = Rows;

        }

        private void SetFlippingBar()
        {
            Rows[7][6] = ACTIVE;
            Rows[7][7] = ACTIVE;
            Rows[7][8] = ACTIVE;

            //store.Add(new Tuple<int, int>(7, 5));
            store.Add(new Tuple<int, int>(7, 6));
            store.Add(new Tuple<int, int>(7, 7));
            store.Add(new Tuple<int, int>(7, 8));
            //store.Add(new Tuple<int, int>(7, 9));

            //store.Add(new Tuple<int, int>(6, 5));
            //store.Add(new Tuple<int, int>(6, 6));
            //store.Add(new Tuple<int, int>(6, 7));
            //store.Add(new Tuple<int, int>(6, 8));
            //store.Add(new Tuple<int, int>(6, 9));

            //store.Add(new Tuple<int, int>(8, 5));
            //store.Add(new Tuple<int, int>(8, 6));
            //store.Add(new Tuple<int, int>(8, 7));
            //store.Add(new Tuple<int, int>(8, 8));
            //store.Add(new Tuple<int, int>(8, 9));
            var l = store.Count;
            for (int i = 0; i < l; i++)
            {
                if (store[i].Item1 - 1 >= 0)
                {
                    //top left
                    if (store[i].Item2 - 1 >= 0) store.Add(new Tuple<int, int>(store[i].Item1 - 1, store[i].Item2 - 1));
                    //top middle
                    store.Add(new Tuple<int, int>(store[i].Item1 - 1, store[i].Item2));
                    //top right
                    if (store[i].Item2 + 1 <= C - 1) store.Add(new Tuple<int, int>(store[i].Item1 - 1, store[i].Item2 + 1));
                }

                //middle left
                if (store[i].Item2 - 1 >= 0) store.Add(new Tuple<int, int>(store[i].Item1, store[i].Item2 - 1));
                //middle right
                if (store[i].Item2 + 1 <= C - 1) store.Add(new Tuple<int, int>(store[i].Item1, store[i].Item2 + 1));

                if (store[i].Item1 + 1 <= R - 1)
                {
                    //bottom left
                    if (store[i].Item2 - 1 >= 0) store.Add(new Tuple<int, int>(store[i].Item1 + 1, store[i].Item2 - 1));
                    //bottom middle
                    store.Add(new Tuple<int, int>(store[i].Item1 + 1, store[i].Item2));
                    //bottom right
                    if (store[i].Item2 + 1 <= C - 1) store.Add(new Tuple<int, int>(store[i].Item1 + 1, store[i].Item2 + 1)); ;
                }
            }
            
        }

        private void SetSmallExploderPattern()
        {
            Rows[6][7] = ACTIVE;
            Rows[7][6] = ACTIVE;
            Rows[7][7] = ACTIVE;
            Rows[7][8] = ACTIVE;
            Rows[8][6] = ACTIVE;
            Rows[8][8] = ACTIVE;
            Rows[9][7] = ACTIVE;
        }

        private void SetGliderPattern()
        {
            Rows[5][7] = ACTIVE;
            store.Add(new Tuple<int, int>(5, 7));
            Rows[6][8] = ACTIVE;
            store.Add(new Tuple<int, int>(6, 8));
            Rows[7][6] = ACTIVE;
            store.Add(new Tuple<int, int>(7, 6));
            Rows[7][7] = ACTIVE;
            store.Add(new Tuple<int, int>(7, 7));
            Rows[7][8] = ACTIVE;
            store.Add(new Tuple<int, int>(7, 8));

        }

        List<StringWrapper> Rows = new List<StringWrapper>();

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new Thread(start).Start();
        }

        private void start()
        {

                List<Tuple<int, int>> trueFlags = new List<Tuple<int, int>>();
                List<Tuple<int, int>> falseFlags = new List<Tuple<int, int>>();

                foreach (var tuple in store)
                {
                    int neighbor_count = 0;

                    if (tuple.Item1 - 1 >= 0)
                    {
                        //top left
                        if (tuple.Item2 - 1 >= 0 && Rows[tuple.Item1 - 1][tuple.Item2 - 1] != DEAD) neighbor_count++;
                        //top middle
                        if (Rows[tuple.Item1 - 1][tuple.Item2] != DEAD) neighbor_count++;
                        //top right
                        if (tuple.Item2 + 1 <= C - 1 && Rows[tuple.Item1 - 1][tuple.Item2 + 1] != DEAD) neighbor_count++;
                    }

                    //middle left
                    if (tuple.Item2 - 1 >= 0 && Rows[tuple.Item1][tuple.Item2 - 1] != DEAD) neighbor_count++;
                    //middle right
                    if (tuple.Item2 + 1 <= C - 1 && Rows[tuple.Item1][tuple.Item2 + 1] != DEAD) neighbor_count++;

                    if (tuple.Item1 + 1 <= R - 1)
                    {
                        //bottom left
                        if (tuple.Item2 - 1 >= 0 && Rows[tuple.Item1 + 1][tuple.Item2 - 1] != DEAD) neighbor_count++;
                        //bottom middle
                        if (Rows[tuple.Item1 + 1][tuple.Item2] != DEAD) neighbor_count++;
                        //bottom right
                        if (tuple.Item2 + 1 <= C - 1 && Rows[tuple.Item1 + 1][tuple.Item2 + 1] != DEAD) neighbor_count++;
                    }

                    if (Rows[tuple.Item1][tuple.Item2] == DEAD)
                    {
                        //Rule for empty cell
                        if (neighbor_count == 3) { Rows[tuple.Item1].Flags[tuple.Item2] = true; trueFlags.Add(new Tuple<int, int>(tuple.Item1, tuple.Item2)); }
                    }
                    else
                    {
                        if (neighbor_count <= 1 || neighbor_count >= 4) { Rows[tuple.Item1].Flags[tuple.Item2] = false; falseFlags.Add(new Tuple<int, int>(tuple.Item1, tuple.Item2)); }
                        else { Rows[tuple.Item1].Flags[tuple.Item2] = true; trueFlags.Add(new Tuple<int, int>(tuple.Item1, tuple.Item2)); }
                    }
                }

                foreach (var flag in trueFlags)
                {
                    Rows[flag.Item1][flag.Item2] = ACTIVE;
                }
                foreach (var flag in falseFlags)
                {
                    Rows[flag.Item1][flag.Item2] = DEAD;
                }

                trueFlags.Clear();
                falseFlags.Clear();
        }

        private void Cell_Click(object sender, MouseButtonEventArgs e)
        {
            DataGridCell cell = (DataGridCell)sender;
            var sw = (StringWrapper)cell.DataContext;

            if ((cell.Background as SolidColorBrush).Color == (Color)ColorConverter.ConvertFromString("White"))
            {
                sw[cell.Column.DisplayIndex] = ACTIVE;
            }
            else
            {
                sw[cell.Column.DisplayIndex] = DEAD;
            }
        }
    }

    public class ValueConverter : IValueConverter
    {
        const string ACTIVE = "a";
        static SolidColorBrush black = new SolidColorBrush((Color)ColorConverter.ConvertFromString("Black"));
        static SolidColorBrush white = new SolidColorBrush((Color)ColorConverter.ConvertFromString("White"));

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((string)value == ACTIVE)
            {
                return black;
            }
            else
            {
                return white;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


}
